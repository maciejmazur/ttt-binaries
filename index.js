// the code is very poor, forgive me.
// this should not be used as a base for the future code, rather an overview of how things can work.

var fs = require("fs");
var Buffer = require("buffer").Buffer;

var originalBinaries = require("./original-binaries.json");



function getOctetFrom (decimal) {
    var binary = decimal.toString(2);
    
    while (binary.length < 8 ) {
        binary = "0" + binary;
    }
    
    return binary;
}

function getDecimalFrom (binary) {
    return parseInt(binary, 2)
}

function getReversedFrom (bufferByte) {
    var octet = getOctetFrom(bufferByte);
    
    var octetReversed = "";
    
    for (j=0; j<octet.length; j++) {
        if (octet[j] === "0") {
            octetReversed += "1";
        } else {
            octetReversed += "0";
        }
    }
    
    return getDecimalFrom(octetReversed);
}

function reverse (buffer, start, length) {
    var reversed = null;
    
    for (i = start; i < start + length; i++) {
        original = buffer[i];
        reversed = getReversedFrom(buffer[i]);
        
        buffer[i] = reversed;
    }
}

function display (buffer, start, bytesToRead, octetsInLine) {
    var bytesAnalysed = "";
    var bytesAnalysedLine = "";
    var bytesReversed = "";
    var bytesReversedLine = "";
    var octet;
    var octetReversed;
        
    for (iteratedByte = 0; iteratedByte <= bytesToRead; iteratedByte++ ) {
        // iterate only on bytes that matter (picture)

        bufferByte = buffer[iteratedByte + start];

        octet = getOctetFrom(bufferByte);
        bytesAnalysedLine += octet;

        if ((iteratedByte+1) % 2 === 0 || iteratedByte >= buffer.length) {
            bytesAnalysed += bytesAnalysedLine + "\n";
            bytesAnalysedLine = "";
        }
    }
    
    console.log(bytesAnalysed);
}



console.log("");

fs.open("test-images/original.lgo", "r", function (status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }
    
    var buffer = new Buffer(128, "binary");
    
    var readOffset = 0;
    var readLength = buffer.length - readOffset;
    
    fs.read(fd, buffer, readOffset, readLength, 0, function (err, bytesRead) {
        
        console.log("bytes original :");
        console.log("");
        
        display(buffer, 18, 32);
        
        reverse(buffer, 18, 32);
        
        fs.writeFile("test-images/manipulated.lgo", buffer, function (err, bytesRead, buffer) {
            console.log(err, bytesRead);
        });
        
    });
});



fs.open("test-images/manipulated.lgo", "r", function (status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }
    
    var buffer = new Buffer(128, "binary");
    
    var readOffset = 0;
    var readLength = buffer.length - readOffset;
    
    fs.read(fd, buffer, readOffset, readLength, 0, function (err, bytesRead) {
        
        console.log("bytes manipulated :");
        console.log("");
        
        display(buffer, 18, 32);
        
    });
});