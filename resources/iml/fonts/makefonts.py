import logging
import os
import sys
import shutil
from optparse import OptionParser

logger = logging.getLogger("makefonts")

def clean():
   logger.info("Cleaning directory")
   if os.path.exists("livr"):
      shutil.rmtree("livr")

   for item in os.listdir("."):
      if os.path.isfile(item) and os.path.splitext(item) in [".fon",".int",".txt",".s",".md5"]:
         os.remove(item)
         
class FontSizeProperty:
   def __init__(self, windowsFontName, pixelToWindowsSizeCoef):
      self.windowsFontName = windowsFontName
      self.pixelToWindowsSizeCoef = float(pixelToWindowsSizeCoef)
      
   def getWindowsSize(self, pixelSize):
      return int(float(pixelSize)*self.pixelToWindowsSizeCoef)

class BallProperty:
   def __init__(self, ballName, fontSizeProperty, characters):
      self.characters=characters
      self.ballName = ballName
      self.outputDir=os.path.join("livr", ballName)
      self.fontSizeProperty=fontSizeProperty

   def generateFonts(self, name, fontPixelSizes, generateFontTxtFile=False):
      def generateFontFile(name, fontSize, isBold, generateFontTxtFile=False):
         logger.info("Generating font file %s (@%s)", name, fontSize)
         #dummy parameter that I don't what it does !!! the reason of this name... sorry
         dummy = 400
         if isBold:
            dummy = 700

         if os.system("""python bmpcar.py -o %s -f "0;%s;0;0;0;%s;0;0;0;0;3;2;1;34;%s" -s %s""" % (name, fontSize, dummy, self.fontSizeProperty.windowsFontName, self.characters)):
            logger.error("Error while generating character's bitmaps")
            sys.exit(1)

         if os.system("""python bmp2fon.py %s""" % (name)):
            logger.error("Error while generating printer font file")
            sys.exit(1)

         if generateFontTxtFile:
            if os.system("""python bmp2txt.py %s > %s.txt""" % (name, name)):
               logger.error("Error while generating text font files")
               sys.exit(1)
         else:
            logger.debug("Characters txt files generation not requested...")

         #remove temporary files
         shutil.rmtree(name)
         
         #copy generated font files
         for item in os.listdir("."):
            if os.path.isfile(item) and item.startswith(name):
               shutil.move(item, self.outputDir)
         
      def generateDefinitionFile(name):
         definitionfileName = "%sDefinition.txt" % name
         logger.info("Generating font definition file %s", definitionfileName)

         handle = open(os.path.join(self.outputDir, definitionfileName), "w")
         handle.write("File automatically generated. DO NOT EDIT !!!\n")
         handle.write("Characters contained in font %s*.fon files are:\n" % (name))
         handle.write(self.characters)
         handle.close()

      if not os.path.exists(self.outputDir):
         os.makedirs(self.outputDir)

      logger.info("Processing ball %s", self.ballName)
      generateDefinitionFile(name)
      for pixelSize in fontPixelSizes:
         fontSize = self.fontSizeProperty.getWindowsSize(pixelSize)

         generateFontFile("%s%02d" % (name, pixelSize),  fontSize, False, generateFontTxtFile)
         generateFontFile("%s%02db" % (name, pixelSize), fontSize, True, generateFontTxtFile)
      
if __name__ == "__main__":
   logger.addHandler(logging.StreamHandler())
   logger.setLevel(logging.INFO)
   
   arialNarrow  = FontSizeProperty("Arial Narrow", -2.67)
   arialUnicode = FontSizeProperty("Arial Unicode MS", -2.40)
   
   # IMPORTANT :
   # Value passed in letteral is extracted from use interface with bmpcar.py, but when a
   # font xxxsize is needed, the value of size char in dialog box may multiply by 2
   # why i don't know, but if we want the equivalence between word print and bmp size,
   # MULTIPLY BY 2 AND THAT'S ALL, DO IT AND SHUT UP !!!!!!!!!!
   #
   # DURING FONT GENERATION, DO NOT USE KEYBOARD AND MOUSE SO THAT FOCUS CHANGES WILL NOT 
   # CORRUPT GENERATED FONTS !!!!
   availableCharacterBalls = {}
   availableCharacterBalls["standard"] = BallProperty("Arial",         arialNarrow, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\u0020\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002A\u002B\u002C\u002D\u002E\u002F\u003A\u003B\u003C\u003D\u003E\u003F\u0040\u005B\u005C\u005D\u005E\u005F\u0060\u007B\u007C\u007D\u007E\u00B0\u00E0\u00E1\u00E2\u00E3\u00E4\u00E7\u00E8\u00E9\u00EA\u00EB\u00EF\u00F1\u00F4\u00F5\u00F6\u00FB\u00FC\u0153\u20AC")
   availableCharacterBalls["extended"] = BallProperty("ArialExtended", arialNarrow, availableCharacterBalls["standard"].characters + "\u00C2\u00C4\u00C7\u00C8\u00C9\u00CA\u00CB\u00CE\u00CF\u00D1\u00D4\u00D6\u00DC")
   availableCharacterBalls["maximum"]  = BallProperty("ArialMaximum",  arialNarrow, availableCharacterBalls["extended"].characters + "\u0081\u00A7\u00A9\u00AD\u00C0\u00C1\u00C3\u00C5\u00C6\u00CC\u00CD\u00D0\u00D2\u00D3\u00D5\u00D7\u00D8\u00D9\u00DA\u00DB\u00DD\u00DE\u00DF\u00E5\u00E6\u00EC\u00ED\u00EE\u00F2\u00F3\u00F7\u00F8\u00F9\u00FA\u00FF\u0152\u0160\u0192\u2014\u201C\u201D\u201E\u2021\u2026\u2030\u2039")

   #removed from arabic for now \u0654 to \u065F \uFE73 \uFE75 \uFEFF
   #arabic set includes:
   #  \u0640,
   #  \u064B to \u0652,
   #  \u066A to \u066C
   #  \uFBFC to \uFC00,
   #  \uFE70, \uFE71, \uFE72, \uFE74, \uFE76 to \uFEFC
   availableCharacterBalls["arabic"] = BallProperty("ArialArabic",     arialUnicode, availableCharacterBalls["standard"].characters + "\u0640\u064B\u064C\u064D\u064E\u064F\u0650\u0651\u0652\u066A\u066B\u066C\uFBFC\uFBFD\uFBFE\uFBFF\uFC00\uFE70\uFE71\uFE72\uFE74\uFE76\uFE77\uFE78\uFE79\uFE7A\uFE7B\uFE7C\uFE7D\uFE7E\uFE7F\uFE80\uFE81\uFE82\uFE83\uFE84\uFE85\uFE86\uFE87\uFE88\uFE89\uFE8A\uFE8B\uFE8C\uFE8D\uFE8E\uFE8F\uFE90\uFE91\uFE92\uFE93\uFE94\uFE95\uFE96\uFE97\uFE98\uFE99\uFE9A\uFE9B\uFE9C\uFE9D\uFE9E\uFE9F\uFEA0\uFEA1\uFEA2\uFEA3\uFEA4\uFEA5\uFEA6\uFEA7\uFEA8\uFEA9\uFEAA\uFEAB\uFEAC\uFEAD\uFEAE\uFEAF\uFEB0\uFEB1\uFEB2\uFEB3\uFEB4\uFEB5\uFEB6\uFEB7\uFEB8\uFEB9\uFEBA\uFEBB\uFEBC\uFEBD\uFEBE\uFEBF\uFEC0\uFEC1\uFEC2\uFEC3\uFEC4\uFEC5\uFEC6\uFEC7\uFEC8\uFEC9\uFECA\uFECB\uFECC\uFECD\uFECE\uFECF\uFED0\uFED1\uFED2\uFED3\uFED4\uFED5\uFED6\uFED7\uFED8\uFED9\uFEDA\uFEDB\uFEDC\uFEDD\uFEDE\uFEDF\uFEE0\uFEE1\uFEE2\uFEE3\uFEE4\uFEE5\uFEE6\uFEE7\uFEE8\uFEE9\uFEEA\uFEEB\uFEEC\uFEED\uFEEE\uFEEF\uFEF0\uFEF1\uFEF2\uFEF3\uFEF4\uFEF5\uFEF6\uFEF7\uFEF8\uFEF9\uFEFA\uFEFB\uFEFC")
   
   fontSizes = [6,7,8,9,10,11,12,14,28,36,40,]
   
   parser = OptionParser()
   parser.add_option("-s", "--sizes", dest="size", default=0, help="Font sizes to generate. Default generates %s font sizes." % fontSizes)
   parser.add_option("-c", "--clean", action="store_true", dest="clean", default=False, help="clean ouput directory and generated files")
   parser.add_option("", "--ball", dest="characterBall", default="standard", help="ball of characters to include in iml5 font file. Available: %s" % availableCharacterBalls.keys())
   parser.add_option("-o", "--out", dest="name", default="arial", help="Warning name shall be less than or equal to 5 characters in length !\nSo that is possible to add font size (2 or characters, i.e.: 40b) and be 8.3 name compliant")
   parser.add_option("", "--txt", action="store_true", dest="generateFontTxtFile", default=False, help="Generate font text files")
   (options, args) = parser.parse_args()
   
   if options.clean:
      clean()
      
   if len(options.name) > 5:
      logger.error("Unexpected name length (%d)" % len(options.name))
      options.print_help()
      sys.exit(1)
      
   if options.size > 99:
      logger.error("Font size should be lower than 99")
      options.print_help()
      sys.exit(1)
      
   if options.characterBall not in availableCharacterBalls.keys():
      logger.error("Unexpected character ball")
      options.print_help()
      sys.exit(1)

   characterBall = availableCharacterBalls[options.characterBall]
   if options.size:
      fontSizes = [options.size]

   logger.warn("""
*********************************************************************************
*********************************************************************************
*********************************************************************************
WARNING: DO NOT USE MOUSE NOR KEYBOARD DURING FONT GENERATION TO AVOID MISTAKES !
*********************************************************************************
*********************************************************************************
*********************************************************************************
""")

   characterBall.generateFonts(options.name, fontSizes, options.generateFontTxtFile)
      
