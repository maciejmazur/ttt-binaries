import getopt
import md5
import os
import sys

def getMd5Sum(filename):
   fd = open(filename, "rb")
   md5sum = md5.new(fd.read() + "FORAFCPERIPHERAL")
   fd.close()
   return md5sum.hexdigest()
   
def readStoredMd5Sum(filename):
   fd = open(filename, "r")
   md5sum = fd.read()
   fd.close()
   return md5sum

def generateMd5File(filename=None, path="."):
   def createMd5File(filename):
      md5sum = getMd5Sum(filename)
      
      md5file = "%s.md5" % filename[:-4]
      handle = open(md5file, "wb")
      handle.write(md5sum)
      handle.close()
      
   filenames = [filename] or os.listdir(path)
   print "generateMd5File on %s" % filenames
   for filename in filenames:
      if os.path.isfile(filename) and filename.endswith(".fon"):
         createMd5File(filename)
         
def checkMd5(filename=None, path="."):
   incorrectMd5 = []
   fontsChecked = False
   
   filenames = [filename] or os.listdir(path)
   print "checkMd5 on %s" % filenames
   for filename in filenames:
      filename = os.path.join(path, filename)
      if os.path.isfile(filename) and filename.endswith(".fon"):
         fontsChecked = True
         currentmd5sum = getMd5Sum(filename)
         storedmd5sum = readStoredMd5Sum("%s.md5" % filename[:-4])
         print "%s:\n\tcurrent: %s\n\tstored : %s" % (filename, currentmd5sum, storedmd5sum)
         if currentmd5sum != storedmd5sum :
            print "\t***** INCORRECT MD5 SUM !!!! *****"
            incorrectMd5.append(os.path.basename(filename))

   if not fontsChecked:
      print "\nNo font files to check"
   elif incorrectMd5:      
      print "\n******ERROR: %d incorrect font file (%s)" % (len(incorrectMd5), incorrectMd5)
   else:
      print "\nFont files are correct"


def usage():
   print """%s <option> <path>
   Options:
      -g : generate individual md5 file for each font file in specified directory
      -c : compare current md5 sum with the one stored in md5 sum file already generated
      -p : path to use as current directory
      -f : font file to treat
   """ % os.path.basename(sys.argv[0])
   sys.exit(0)

if __name__ == "__main__":    
   (opts, params) = getopt.getopt(sys.argv[1:], "gcpf:")

   path = "."
   functor = None
   filename = None
   
   for opt in opts :
      (key, value) = opt
      if key == "-g":
         functor = generateMd5File
      elif key == "-c":
         functor = checkMd5
      elif key == "-p":
         path = value
      elif key == "-f":
         filename = value
   if not functor:
      usage()
      
   #print functor, filename, os.path.abspath(path)
   functor(filename, os.path.abspath(path))
      
   
         

