#!/usr/bin/python

import getopt, os, string, sys
import wx

version = '0.1'

class MyDialog(wx.Dialog):
	def __init__(self, parent):
		pre = wx.PreDialog()
		pre.Create(parent, title="BmpCar dialog", size=[250, 110])
		self.PostCreate(pre)

		bs_main = wx.BoxSizer(orient=wx.VERTICAL)

		self.cb_upper = wx.CheckBox(self, label="Upper")
		bs_main.Add(self.cb_upper, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		self.cb_lower = wx.CheckBox(self, label="Lower")
		bs_main.Add(self.cb_lower, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		self.cb_digit = wx.CheckBox(self, label="Digit")
		bs_main.Add(self.cb_digit, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		text1 = wx.StaticText(self, label="Other characters :")
		bs_main.Add(text1, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		self.tc_other_char = wx.TextCtrl(self, size=[200,-1])
		bs_main.Add(self.tc_other_char, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		sb_output = wx.StaticBox(self)
		bs_output = wx.StaticBoxSizer(sb_output, orient=wx.HORIZONTAL)

		text2 = wx.StaticText(self, label="Output (directory)")
		bs_output.Add(text2, flag=wx.ALIGN_CENTRE|wx.ALL, border=5)

		self.tc_output = wx.TextCtrl(self, size=[80,-1])
		bs_output.Add(self.tc_output, flag=wx.ALIGN_CENTRE|wx.ALL, border=5)

		bs_main.Add(bs_output, flag=wx.ALIGN_CENTER_VERTICAL|wx.ALL, border=5)

		bs_buttons = wx.BoxSizer(orient=wx.HORIZONTAL)

		b_ok = wx.Button(self, id=wx.ID_OK, label="OK")
		b_ok.SetDefault()
		bs_buttons.Add( b_ok, flag=wx.ALIGN_CENTRE|wx.ALL, border=5)

		b_cancel = wx.Button(self, id=wx.ID_CANCEL, label="Cancel")
		bs_buttons.Add(b_cancel, flag=wx.ALIGN_CENTRE|wx.ALL, border=5)

		bs_main.Add(bs_buttons, flag=wx.ALIGN_CENTRE|wx.ALL, border=5)

		self.SetAutoLayout(True)
		self.SetSizer(bs_main)
		bs_main.Fit(self)
		bs_main.SetSizeHints(self)

class MyFrame(wx.Frame):
	def __init__(self):
#		self.config = wx.ConfigBase.Get()
		self.font = None
		title = "BmpCar"
		wx.Frame.__init__(self, None, -1, title)

		self.SetPosition([20, 20])
		self.SetSize([500, 340])
		self.SetSizeHints(500, 340)	# fixe minimum
#		icon = images.getMondrianIcon()
#		self.SetIcon(icon)

		self.AddMenu()
		self.CreateStatusBar()
		self.SetBackgroundColour(wx.WHITE)

	def AddMenu(self):
		bar = wx.MenuBar()

		file = wx.Menu()
		file.Append(wx.ID_PRINT, "G&o")
		file.Append(wx.ID_EXIT, "E&xit")

		help = wx.Menu()
		help.Append(wx.ID_ABOUT, "&About...")

		bar.Append(file, "&File")
		bar.Append(help, "&Help")

		self.SetMenuBar(bar)

		self.Bind(wx.EVT_MENU, self.OnClose, id=wx.ID_EXIT)
		self.Bind(wx.EVT_MENU, self.OnGo, id=wx.ID_PRINT)
		self.Bind(wx.EVT_MENU, self.OnAbout, id=wx.ID_ABOUT)
		self.Bind(wx.EVT_CLOSE, self.OnClose)

	def OnAbout(self, evt):
		py_version = sys.version.split()[0]
		msg = """BmpCar, version %s
(c) 2005 Parkeon SAS, Francois Perrad
wxPython %s
Python %s""" % (version, wx.__version__, py_version)
		wx.MessageBox(message=msg, caption="About ...", style=wx.OK | wx.CENTRE)

	def OnClose(self, evt):
		self.Destroy()

	def OnGo(self, evt):
		str_weight = { wx.NORMAL: "", wx.LIGHT: " Light", wx.BOLD: " Bold" }
		str_style = { wx.NORMAL: "", wx.SLANT: " Slant", wx.ITALIC: " Italic" }
		font_data = wx.FontData()
		if self.font != None :
			font_data.SetInitialFont(self.font)
		font_data.EnableEffects(0)	# disable
		dialog = wx.FontDialog(self, font_data)
		if dialog.ShowModal() == wx.ID_OK :
			font = dialog.GetFontData().GetChosenFont()
			if font != None :
				self.font = font
				#print font.GetNativeFontInfoDesc()

				self.str_font = "%s%s%s %d" % (
					font.GetFaceName(),
					str_weight[font.GetWeight()],
					str_style[font.GetStyle()],
					font.GetPointSize()
				)
				self.SetStatusText(self.str_font, 0)
				my_dialog = MyDialog(self)
				my_dialog.CenterOnScreen()
				if my_dialog.ShowModal() == wx.ID_OK :
					upper = my_dialog.cb_upper.GetValue()
					lower = my_dialog.cb_lower.GetValue()
					digit = my_dialog.cb_digit.GetValue()
					others = my_dialog.tc_other_char.GetValue()
					output = my_dialog.tc_output.GetValue()
					self.Process(upper, lower, digit, others, output)
				my_dialog.Destroy()
		dialog.Destroy()

	def Process(self, upper, lower, digit, others, output):
		self.max_width = 0
		self.max_height = 0

		dc = wx.ClientDC(self)
		dc.SetFont(self.font)

		if output != "" :
			if os.access(output, os.F_OK) != True :
				os.mkdir(output)

		if digit == True :
			for ch in string.digits :
				self.ProcessChar(dc, ch, output)
		if upper == True :
			for ch in string.ascii_uppercase :
				self.ProcessChar(dc, ch, output)
		if lower == True :
			for ch in string.ascii_lowercase :
				self.ProcessChar(dc, ch, output)
		if others != "" :
			for ch in others :
				self.ProcessChar(dc, ch, output)

		str = "%s : (%d x %d)" % (self.str_font, self.max_width, self.max_height)
		if output != "" :
			str += " -> " + output
		self.SetStatusText(str, 0)

	def Batch(self, font, set, output):
		self.max_width = 0
		self.max_height = 0
		dc = wx.ClientDC(self)
		dc.SetFont(font)
		for ch in set :
			self.ProcessChar(dc, ch, output)

	def ProcessChar(self, dc, ch, output):
		outfile = "%04x.bmp" % ord(ch)
		if output != "" :
			outfile = output + "/" + outfile

		dc.Clear()
		dc.DrawText(ch, 0, 0)
		(width, height) = dc.GetTextExtent(ch)
		self.max_width = max(self.max_width, width)
		self.max_height = max(self.max_height, height)

		bitmap = wx.EmptyBitmap(width, height)
		temp_dc = wx.MemoryDC()
		temp_dc.SelectObject(bitmap)
		temp_dc.Blit(0, 0, width, height, dc, 0, 0)
		bitmap.SaveFile(outfile, type=wx.BITMAP_TYPE_BMP)

class MyApp(wx.App):
	def OnInit(self):
		(opts, params) = getopt.getopt(sys.argv[1:], "o:f:s:")
		wx.InitAllImageHandlers()
		frame = MyFrame()
		frame.Show(True)		
		self.SetTopWindow(frame)
		if len(opts) > 0 :
			output = ""
			font = None
			set = ""
			for opt in opts :
				(key, value) = opt
				if key == "-o" :
					output = value
					if os.access(output, os.F_OK) != True :
						os.mkdir(output)
				elif key == "-f" :
					font = wx.FontFromNativeInfoString(value)
					#print "set Font %s" %font
				elif key == "-s" :
					set = eval("u'%s'" % value)
			if font != None and set != "" :
				frame.Batch(font, set, output)
				sys.exit(0)
		return True

if __name__ == '__main__':
	app = MyApp(False)
	app.MainLoop()
