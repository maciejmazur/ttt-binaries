#!/usr/bin/python

import getopt, os, os.path, re, sys, struct, md5
import Image

def bmp2fon(fontname, coef):
    outfile = fontname + ".fon"
    f = open(outfile, mode='wb')
    dir = os.listdir(fontname)
    filter = re.compile("([0-9A-Fa-f]{2,4})\.bmp")
    bank = {}
    for filename in dir :
        capt = filter.match(filename)
        if capt :
            ch = eval("0x" + capt.group(1))
            try :
#               print "Open bmp name : %s" %(filename)
                img = Image.open(fontname + "/" + filename)
                if img.format != "BMP" :
                    print "Bad format: ", filename, img.format
                else :
                    cell = ch / 256
                    if bank.has_key(cell) == False :
                        bank[cell] = {}
                    bank[cell][ch] = img
            except IOError:
                print "can't open %s" % filename
    version = list(fontname)
    while len(version) < 12 :
        version.append(chr(0))
    version = version[:12]  #limit length to 12 characters
    version.append('F')
    version.append('O')
    version.append('N')
    version.append('T')
    f.write(struct.pack('16c', *version))
    keys_cell = bank.keys()
    keys_cell.sort()
    for cell in keys_cell :
        max_width = 0
        max_height = 0
        keys_char = bank[cell].keys()
        keys_char.sort()
        for ch in keys_char :
            img = bank[cell][ch]
            width = img.size[0]
            height = img.size[1]
#           print "ImageSize : %d, %d"%(width, height)
            if coef != 1 :
                #print "+++++ resized !!!!"
                width = int(width * coef)
                bank[cell][ch] = img.resize((width, height))
            max_width = max(max_width, width)
            max_height = max(max_height, height)
        height16 = (max_height + 15) / 16
        nb_char = 0
        indirect256 = [0]
        indirect256 *= 256
        width256 = [0] * 256
        data = [0]
        data_size = len(keys_char) * 2 * height16 * max_width
        padding_size = 0
#       print "GRP data_size %d" %(data_size)
        if data_size % 4 :
            padding_size = data_size % 4
#           print "Padding size %d" %(padding_size)
        data *= (data_size + padding_size)
        ptr = 0
        for ch in keys_char :
            #print "Character 'x%x,x%x'" % (cell, ch)
            img = bank[cell][ch].convert("L")   # convert to greyscale
            (width, height) = img.size
            width256[ch % 256] = width
            indirect256[ch % 256] = nb_char
            for w in range(width) :
                for h in range(height) :
                    pix = img.getpixel((w, height - 1 - h))
                    #print (w, h, pix)
                    #add pixels to font character if grey level lower than trigger defined
                    if pix < 200 :  #if pix == (0, 0, 0) or pix == 0 :
                        data[ptr + h/8] |= 0x01 << (h % 8)
                ptr += 2 * height16
            ptr += 2 * height16 * (max_width - width)
            nb_char += 1
        f.write(struct.pack('H', cell >> 8))
        f.write(struct.pack('H', (cell % 256) << 8))
        offsetNextBMP = max_width * height16 * nb_char + padding_size / 2
#       print "offsetNextBMP %d (%d, %d, %d)" %(offsetNextBMP, max_width ,height16 ,nb_char)
        f.write(struct.pack('L', offsetNextBMP))
        f.write(struct.pack('H', max_width))
        f.write(struct.pack('H', max_height))
        f.write(struct.pack('H', max_width * height16))
        f.write(struct.pack('H', height16))
        f.write(struct.pack('256B', *indirect256))
        f.write(struct.pack('256B', *width256))
        fmt = str(len(data)) + 'B'
        f.write(struct.pack(fmt, *data))
    f.write(struct.pack('H', 0))
    f.write(struct.pack('H', 0))
    f.close()

def createMd5File(filename):
    fd = open("%s.fon" % filename, "rb")
    md5sum = md5.new(fd.read() + "FORAFCPERIPHERAL")
    fd.close()
    md5file = "%s.md5" % filename
    handle = open(md5file, "wb")
    handle.write(md5sum.hexdigest())
    handle.close()

def usage():
    print "%s filename" % os.path.basename(sys.argv[0])
    sys.exit(0)

if __name__ == '__main__':
    (opts, params) = getopt.getopt(sys.argv[1:], "Hhc:")
    coef = 1
    for opt in opts :
        (key, value) = opt
        if key == "-H" or key == "-h" :
            usage()
        elif key == "-c" :
            coef = eval(value)
    if len(params) != 1 :
        usage()
    bmp2fon(params[0], coef)
    createMd5File(params[0])

