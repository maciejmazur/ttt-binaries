#!/usr/bin/perl

use strict;
use Wx;

# constants
use vars qw($ID_BUTTON_OK); $ID_BUTTON_OK = 10000;
use vars qw($ID_BUTTON_CANCEL); $ID_BUTTON_CANCEL = 10001;
use vars qw($ID_UPPER); $ID_UPPER = 10002;
use vars qw($ID_LOWER); $ID_LOWER = 10003;
use vars qw($ID_DIGIT); $ID_DIGIT = 10004;
use vars qw($ID_TEXT); $ID_TEXT = 10005;
use vars qw($ID_OTHER_CHAR); $ID_OTHER_CHAR = 10006;
use vars qw($ID_OUTPUT); $ID_OUTPUT = 10007;

use vars qw($ID_ABOUT); $ID_ABOUT = 10008;
use vars qw($ID_GO); $ID_GO = 10009;
use vars qw($ID_QUIT); $ID_QUIT = 10010;


package MyDialog;

use strict;
use base qw(Wx::Dialog);

use Wx::Event qw(EVT_CLOSE EVT_BUTTON);
use Wx qw( wxDefaultSize wxDefaultPosition wxID_OK wxID_CANCEL wxID_YES );
use Wx qw( wxVERTICAL wxHORIZONTAL wxALL wxLEFT wxRIGHT wxTOP wxBOTTOM wxCENTRE wxGROW );
use Wx qw( wxALIGN_RIGHT wxALIGN_BOTTOM wxALIGN_CENTRE wxALIGN_CENTER_VERTICAL wxALIGN_CENTER_HORIZONTAL );

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(undef, -1, "BmpCar dialog", wxDefaultPosition, [250, 110]);

    $self->MyDialogFunc();

    EVT_BUTTON($self, $self->{b_ok}, \&OnOk);
    EVT_BUTTON($self, $self->{b_cancel}, \&OnCancel);

    return $self;
}

sub MyDialogFunc {
    my $self = shift;

    my $bs_main = new Wx::BoxSizer(wxVERTICAL);

    $self->{cb_upper} = new Wx::CheckBox($self, $main::ID_UPPER, "Upper", wxDefaultPosition, wxDefaultSize, 0);
    $bs_main->AddWindow($self->{cb_upper}, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    $self->{cb_lower} = new Wx::CheckBox($self, $main::ID_LOWER, "Lower", wxDefaultPosition, wxDefaultSize, 0);
    $bs_main->AddWindow($self->{cb_lower}, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    $self->{cb_digit} = new Wx::CheckBox($self, $main::ID_DIGIT, "Digit", wxDefaultPosition, wxDefaultSize, 0);
    $bs_main->AddWindow($self->{cb_digit}, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    my $text1 = new Wx::StaticText($self, $main::ID_TEXT, "Other characters :", wxDefaultPosition, wxDefaultSize, 0);
    $bs_main->AddWindow($text1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    $self->{tc_other_char} = new Wx::TextCtrl($self, $main::ID_OTHER_CHAR, "", wxDefaultPosition, [200,-1], 0);
    $bs_main->AddWindow($self->{tc_other_char}, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    my $sb_output = new Wx::StaticBox($self, -1, "");
    my $bs_output = new Wx::StaticBoxSizer($sb_output, wxHORIZONTAL);

    my $text2 = new Wx::StaticText($self, $main::ID_TEXT, "Output (directory)", wxDefaultPosition, wxDefaultSize, 0);
    $bs_output->AddWindow($text2, 0, wxALIGN_CENTRE|wxALL, 5);

    $self->{tc_output} = new Wx::TextCtrl($self, $main::ID_OUTPUT, "", wxDefaultPosition, [80,-1], 0);
    $bs_output->AddWindow($self->{tc_output}, 0, wxALIGN_CENTRE|wxALL, 5);

    $bs_main->Add($bs_output, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    my $bs_buttons = new Wx::BoxSizer(wxHORIZONTAL);

    $self->{b_ok} = new Wx::Button($self, $main::ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
    $self->{b_ok}->SetDefault();
    $bs_buttons->AddWindow( $self->{b_ok}, 0, wxALIGN_CENTRE|wxALL, 5);

    $self->{b_cancel} = new Wx::Button($self, $main::ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
    $bs_buttons->AddWindow($self->{b_cancel}, 0, wxALIGN_CENTRE|wxALL, 5);

    $bs_main->Add($bs_buttons, 0, wxALIGN_CENTRE|wxALL, 5);

    $self->SetAutoLayout(1);
    $self->SetSizer($bs_main);
    $bs_main->Fit($self);
    $bs_main->SetSizeHints($self);
}

sub OnCancel {
    my($self, $event) = @_;

    $self->EndModal($main::ID_BUTTON_CANCEL);
}

sub OnOk {
    my($self, $event) = @_;

    $self->EndModal($main::ID_BUTTON_OK);
}

package MyFrame;

use strict;
use base qw(Wx::Frame);

use Wx::Event qw(EVT_MENU EVT_CLOSE EVT_SIZE EVT_UPDATE_UI EVT_PAINT);
use Wx qw(wxOK wxCENTRE wxID_CANCEL wxWHITE wxBITMAP_TYPE_BMP);
use Wx qw(wxNORMAL wxLIGHT wxBOLD wxSLANT wxITALIC);

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    $self->CreateMyMenuBar();

    $self->CreateStatusBar(1);

    $self->SetBackgroundColour(wxWHITE);

    $self->SetIcon(Wx::GetWxPerlIcon());

    $self->{font} = undef;

    EVT_MENU($self, $main::ID_ABOUT, \&OnAbout);
    EVT_MENU($self, $main::ID_QUIT, \&OnQuit);
    EVT_MENU($self, $main::ID_GO, \&OnGo);
    EVT_CLOSE($self, \&OnCloseWindow);

    return $self;
}

sub CreateMyMenuBar {
    my $self = shift;

    my $file_menu = new Wx::Menu();
    $file_menu->Append( $main::ID_ABOUT, "About...", "Program info");
    $file_menu->Append( $main::ID_GO, "Go", "Execute program");
    $file_menu->Append( $main::ID_QUIT, "Quit", "Quit program");

    my $menu_bar = new Wx::MenuBar();
    $menu_bar->Append($file_menu, "File");

    $self->SetMenuBar($menu_bar);
}

# handler implementations for MyFrame

sub OnAbout {
    my($self, $event) = @_;

    Wx::MessageBox("BmpCar v0.01 (wxPerl)\n(c) 2003",
                   "About...", wxOK|wxCENTRE, $self);
}

sub OnQuit {
    my($self, $event) = @_;

    $self->Close(1);
}

sub OnCloseWindow {
    my($self, $event) = @_;

    $self->Destroy();
}

sub OnGo {
    my($self, $event) = @_;
    my %str_weight = ( eval(wxNORMAL) => "", eval(wxLIGHT) => " Light", eval(wxBOLD) => " Bold" );
    my %str_style = ( eval(wxNORMAL) => "", eval(wxSLANT) => " Slant", eval(wxITALIC) => " Italic" );

    my $font_data = new Wx::FontData();
    $font_data->SetInitialFont($self->{font}) if ($self->{font});
    $font_data->EnableEffects(0); # disable
    my $dialog = new Wx::FontDialog($self, $font_data);

    if ($dialog->ShowModal() == wxID_CANCEL) {
        $dialog->Destroy();
    } else {
        my $font = $dialog->GetFontData()->GetChosenFont();
        $dialog->Destroy();

        if ($font) {
            $self->{font} = $font;
            $self->{str_font} = $font->GetFaceName() . $str_weight{$font->GetWeight()};
            $self->{str_font} .= $str_style{$font->GetStyle()} . " " . $font->GetPointSize();
            $self->SetStatusText($self->{str_font}, 0);
            my $my_dialog = new MyDialog();
            if ($my_dialog->ShowModal() != $main::ID_BUTTON_OK) {
                $my_dialog->Destroy();
            } else {
                my $upper = $my_dialog->{cb_upper}->GetValue();
                my $lower = $my_dialog->{cb_lower}->GetValue();
                my $digit = $my_dialog->{cb_digit}->GetValue();
                my $others = $my_dialog->{tc_other_char}->GetValue();
                my $output = $my_dialog->{tc_output}->GetValue();
                $my_dialog->Destroy();
                $self->Process($upper, $lower, $digit, $others, $output);
            }
        }

    }
}

sub Process {
    my $self = shift;
    my($upper, $lower, $digit, $others, $output) = @_;

    $self->{max_width} = 0;
    $self->{max_height} = 0;

    my $dc = new Wx::ClientDC($self);

    # select the font
    $dc->SetFont($self->{font});

    if ($output) {
        unless (-d $output) {
            mkdir $output;
        }
    }

    if ($upper) {
        foreach my $ch ("A" ..  "Z") {
            $self->ProcessChar($dc, $ch, $output);
        }
    }
    if ($lower) {
        foreach my $ch ("a" ..  "z") {
            $self->ProcessChar($dc, $ch, $output);
        }
    }
    if ($digit) {
        foreach my $ch ("0" ..  "9") {
            $self->ProcessChar($dc, $ch, $output);
        }
    }
    if ($others) {
        foreach my $ch (split //, $others) {
            $self->ProcessChar($dc, $ch, $output);
        }
    }

    my $str = $self->{str_font} . " : (" . $self->{max_width} . " x " . $self->{max_height} . ")";
    $str .= " -> $output" if ($output);
    $self->SetStatusText($str, 0);
}

sub ProcessChar {
    my $self = shift;
    my($dc, $ch, $output) = @_;

    my $outfile = sprintf "%02x.bmp", ord($ch);
    $outfile = $output . "/" . $outfile if ($output);

    $dc->Clear();
    $dc->DrawText($ch, 0, 0);
    my ($width, $height) = $dc->GetTextExtent($ch);
    $self->{max_width} = $width if ($width > $self->{max_width});
    $self->{max_height} = $height if ($height > $self->{max_height});

    my $bitmap = new Wx::Bitmap($width, $height);
    my $temp_dc = new Wx::MemoryDC();
    $temp_dc->SelectObject($bitmap);
    $temp_dc->Blit(0, 0, $width, $height, $dc, 0, 0);
    $bitmap->SaveFile($outfile, wxBITMAP_TYPE_BMP);
}

package MyApp;

use strict;
use base qw(Wx::App);

# this is called automatically on object creation
sub OnInit {
    my $self = shift;

    # create a new frame
    my $frame = new MyFrame(undef, -1, "BmpCar", [20,20], [500,340]);

    # set as top frame
    $self->SetTopWindow($frame);
    # show it
    $frame->Show(1);
}

package main;

my $app = new MyApp();
$app->MainLoop();

