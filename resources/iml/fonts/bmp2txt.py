#!/usr/bin/python

import getopt, os, os.path, re, sys
import Image

def bmp2txt(fontname, coef):
	dir = os.listdir(fontname)
	filter = re.compile("([0-9A-Fa-f]{2,4})\.bmp")
	imgs = {}
	for filename in dir :
		capt = filter.match(filename)
		if capt :
			ch = eval("0x" + capt.group(1))
			try :
				img = Image.open(fontname + "/" + filename)
				if img.format != "BMP" :
					print "Bad format: ", filename, img.format
				else :
					imgs[ch] = img
			except IOError:
				print "can't open %s" % filename
	keys = imgs.keys()
	keys.sort()
	for c in keys :
		img = imgs[c]
		(width, height) = img.size
		if coef != 1 :
			img = img.resize((int(width * coef), height))
			(width, height) = img.size
		print "* " + hex(c)
		for w in range(width) :
			str = ""
			for h in range(height)[::-1] :
				pix = img.getpixel((w, h))
#				print (w, h, pix)
				if pix == (0, 0, 0) or pix == 0:
					str += "#"
				else :
					str += "-"
			print str
	print "END"

def usage():
	print "%s filename" % os.path.basename(sys.argv[0])
	sys.exit(0)

if __name__ == '__main__':
	(opts, params) = getopt.getopt(sys.argv[1:], "Hhc:")
	coef = 1
	for opt in opts :
		(key, value) = opt
		if key == "-H" or key == "-h" :
			usage()
		elif key == "-c" :
			coef = eval(value)
	if len(params) != 1 :
		usage()
	bmp2txt(params[0], coef)
