#!/usr/bin/python

import getopt, os.path, sys, struct
import Image

def bmp2lgo(filename):
    try :
        if not filename.endswith(".bmp"):
            filename += ".bmp"
        img = Image.open(filename)
        print img.mode
        if img.format != "BMP" :
            print "%s is not a BMP file (%s file format)" % (filename, img.format)
    except IOError:
        print "can't open %s" % filename
        sys.exit(1)
    img = img.convert("1")
    filename = os.path.basename(filename)
    version = list("%-11.11s" % filename)
    version.append(chr(0))
    version.append('O')
    version.append('G')
    version.append('O')
    version.append('L')
    print version
    width = img.size[0]
    height = img.size[1]
    height16 = (height + 15) / 16
    data = [0]
    data *= 2 * height16 * width
    ptr = 0
    for w in range(width) :
        for h in range(height) :
            pix = img.getpixel((w, height - 1 - h))
#			print (w, h, pix)
            if pix == (0, 0, 0) or pix == 0 :
                data[ptr + h/8] |= 0x01 << (h % 8)
        ptr += 2 * height16

    outfile = filename + ".lgo"
    f = open(outfile, mode='wb')
    f.write(struct.pack('16c', *version))
    f.write(struct.pack('H', width))
    f.write(struct.pack('H', height))
    f.write(struct.pack('H', width * height16))
    f.write(struct.pack('H', height16))
    fmt = str(len(data)) + 'B'
    f.write(struct.pack(fmt, *data))
    f.close()

def usage():
    print "%s filename" % os.path.basename(sys.argv[0])
    sys.exit(0)

if __name__ == '__main__':
    (opts, params) = getopt.getopt(sys.argv[1:], "Hh")
    for opt in opts :
        (key, value) = opt
        if key == "-H" or key == "-h" :
            usage()
    if len(params) != 1 :
        usage()
    bmp2lgo(params[0])
