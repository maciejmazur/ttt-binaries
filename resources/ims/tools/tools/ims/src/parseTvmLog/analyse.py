import re, os


#####################################################
#####################################################
#####################################################
class fileExtract:
   def __init__ (self, outFile="", fmt="", mode="w"):
      self.extractLineFormat=""
      self.outFile = outFile
      self.extractLineFormat=fmt
      self.interp_dict = dict()
      self.mode=mode

   def setExtractLineFormat(self, fmt):
      self.extractLineFormat=fmt
      
   def extractLineData (self, fmt=None, newline=1):
      if newline:
         self.line = self.inFileId.readline()
      if self.line is not '':
         if fmt is None:
            return re.match(self.extractLineFormat, self.line)
         else:
            return re.match(fmt, self.line)

      else:
         raise EOFError

   def recordOutputData (self, regInstance):
      raise NotImplementedError

   def resetInternalVars (self):
      raise NotImplementedError

   def processFile (self, inFile, outFile=None):
      self.inFileId = file(inFile)
      if outFile:
         self.outputFileId = open(outFile, self.mode)
      else:
         self.outputFileId = open(self.outFile, self.mode)

      self.outputFileId.write("Processing file '%s', mode '%s'\n" % (inFile, self.mode))
      self.resetInternalVars()

      lineNb=0

      while(1):
         try:
            lineNb += 1
            reg = self.extractLineData()
            if reg is not None:
               self.recordOutputData(reg)
            else:
               print "File %s, No data to extract on line %d" % (inFile, lineNb)
         except EOFError:
            print "File '%s' Processed !" % inFile
            break

      self.inFileId.close()
      self.outputFileId.close()

   def set_inter_rule(self, data_type, src, dest):
         if not self.interp_dict.has_key(data_type):
            self.interp_dict[data_type] = dict()

         self.interp_dict[data_type][src] = dest;

   def interp_output_data(self, data_type, data_value):
      if self.interp_dict.has_key(data_type):
         if self.interp_dict[data_type].has_key(data_value):
            return self.interp_dict[data_type][data_value]

      return data_value

#####################################################
#####################################################
#####################################################
class filesExtract(fileExtract):
   def __init__ (self, outFile="", fmt=""):
      fileExtract.__init__(self, outFile, fmt, mode="a")

   def processFiles(self, inFileFmt):
      dirEntriesList = os.listdir(os.curdir)
      flag=0

      self.outputFileId = open(self.outFile, "w")
      self.outputFileId.close()

      for entry in dirEntriesList:
         if os.path.isfile(entry):
            if re.match(inFileFmt, entry):
               flag+=1
               self.processFile(entry)
      if not flag:
         raise AttributeError, "No file names matching provided file name format !!!"

#####################################################
#####################################################
#####################################################
class hdlcLogFile(filesExtract):
   def __init__ (self, outFile, fmt=""):
      filesExtract.__init__(self, outFile, fmt) 
      self.startBufferDataLineFmt="^[0-9]{2}:[0-9]{2}:[0-9]{2} CHDLCCtrl::(?P<in_out>Send|OnReceive)\(.*"
      self.bufferDataLineFmt  ="^[0-9]{2}:[0-9]{2}:[0-9]{2} (?P<data>([0-9a-fA-F]{2}\s*){1,16}.*)"

      self.setExtractLineFormat("^(?P<who>.{3}),.*,(?P<data>.{2})")
      self.set_inter_rule("who", "DTE", "Host")
      self.set_inter_rule("who", "DCE", "CF79")

   def processFile (self, inFile, outFile=None):
      lineNb = 0
      self.inFileId = file(inFile)
      if outFile:
         self.outputFileId = open(outFile, self.mode)
      else:
         self.outputFileId = open(self.outFile, self.mode)

      self.outputFileId.write("\n*\n*\n* Processing file '%s', mode '%s'\n*\n*\n\n" % (inFile, self.mode))

      while(1):
         try:
            lineNb += 1
            reg = self.extractLineData(self.startBufferDataLineFmt)
            if reg is not None:
               self.outputFileId.write("Line %d - %s :\n" % (lineNb, reg.group("in_out")))
            else:
               reg = self.extractLineData(self.bufferDataLineFmt, 0)
               if reg is not None:
                  self.outputFileId.write("%s :\n" % (reg.group("data")))
         except EOFError:
            print "File '%s' Processed !" % inFile
            break

      self.inFileId.close()
      self.outputFileId.close()

#####################################################
#####################################################
#####################################################
class mdbSpyFile(fileExtract):
   def __init__ (self, outFile, fmt=""):
      fileExtract.__init__(self, outFile, fmt) 
      self.resetInternalVars()
      self.setExtractLineFormat("^(?P<who>.{3}),.*,(?P<data>.{2})")
      self.set_inter_rule("who", "DTE", "Host")
      self.set_inter_rule("who", "DCE", "CF79")

   def resetInternalVars(self):
      self.whois=""
      self.datas=""

   def recordOutputData (self, regInstance):
      if self.whois != regInstance.group("who"):
         if self.whois is not "":
            self.outputFileId.write("%s : %s\n" % (self.interp_output_data("who", self.whois), self.interp_output_data("data", self.datas)))
         self.whois=regInstance.group("who")
         self.datas=regInstance.group("data")
      else:
         self.datas+="," + regInstance.group("data")


            
#####################################################
#####################################################
#####################################################
if __name__ == '__main__':
   log = hdlcLogFile("analyse_hdlc.log")
   log.processFiles("hdlc.*log")    

