// BmpCarView.h : interface of the CBmpCarView class
//
/////////////////////////////////////////////////////////////////////////////

class CBmpCarView : public CView
{
protected: // create from serialization only
	CBmpCarView();
	DECLARE_DYNCREATE(CBmpCarView)

// Attributes
public:
	CBmpCarDoc* GetDocument();
	CString m_Carac;
	LOGFONT	m_lf;
	CSize	m_TotalSize;
	
// Operations
public:
	void		BITMAPINFOtoFile( const CString& FileName, const BITMAPFILEHEADER& BitmapFileHeader, BITMAPINFO* BitmapInfo, LPBYTE MemoryBits, int NbColors );
	LPBYTE		GetDIBBits( const CDC& MemoryDC, BITMAPINFO* BitmapInfo, const CBitmap& MemoryBmp );
	PBITMAPINFO	InitBITMAPINFO( const CBitmap& MemoryBmp, int& NbColors );
	void		InitBITMAPFILEHEADER( BITMAPFILEHEADER& BitmapFileHeader, BITMAPINFO* BitmapInfo, int NbColors );
	void		SavePageToBmp( const CString& FileName );
	void		SaveDC( CDC* MemoryDC, CBitmap* MemoryBmp );


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBmpCarView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBmpCarView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CBmpCarView)
	afx_msg void OnGo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in BmpCarView.cpp
inline CBmpCarDoc* CBmpCarView::GetDocument()
   { return (CBmpCarDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
