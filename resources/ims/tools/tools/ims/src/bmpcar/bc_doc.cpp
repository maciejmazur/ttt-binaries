// BmpCarDoc.cpp : implementation of the CBmpCarDoc class
//

#include "stdafx.h"
#include "BmpCar.h"

#include "BC_Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBmpCarDoc

IMPLEMENT_DYNCREATE(CBmpCarDoc, CDocument)

BEGIN_MESSAGE_MAP(CBmpCarDoc, CDocument)
	//{{AFX_MSG_MAP(CBmpCarDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBmpCarDoc construction/destruction

CBmpCarDoc::CBmpCarDoc()
{
	// TODO: add one-time construction code here

}

CBmpCarDoc::~CBmpCarDoc()
{
}

BOOL CBmpCarDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CBmpCarDoc serialization

void CBmpCarDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBmpCarDoc diagnostics

#ifdef _DEBUG
void CBmpCarDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBmpCarDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBmpCarDoc commands
