// BmpCarView.cpp : implementation of the CBmpCarView class
//

#include "stdafx.h"
#include "BmpCar.h"

#include "BC_Doc.h"
#include "BC_View.h"
#include "MainFrm.h"
#include <math.h>
#include "ChCar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView

IMPLEMENT_DYNCREATE(CBmpCarView, CView)

BEGIN_MESSAGE_MAP(CBmpCarView, CView)
	//{{AFX_MSG_MAP(CBmpCarView)
	ON_COMMAND(ID_GO, OnGo)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView construction/destruction

CBmpCarView::CBmpCarView()
{
	// TODO: add construction code here

}

CBmpCarView::~CBmpCarView()
{
}

BOOL CBmpCarView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView drawing

void CBmpCarView::OnDraw(CDC* pDC)
{
	CBmpCarDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CFont	font;
	CFont	*pOldFont;
	font.CreateFontIndirect( &m_lf );
	pOldFont = pDC->SelectObject( &font );
	pDC->TextOut( 0,0,m_Carac);
	m_TotalSize = pDC->GetTextExtent( m_Carac );
	pDC->SelectObject( pOldFont );
}

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView printing

BOOL CBmpCarView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CBmpCarView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CBmpCarView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView diagnostics

#ifdef _DEBUG
void CBmpCarView::AssertValid() const
{
	CView::AssertValid();
}

void CBmpCarView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBmpCarDoc* CBmpCarView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBmpCarDoc)));
	return (CBmpCarDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBmpCarView message handlers

void CBmpCarView::OnGo() 
{
	CHOOSEFONT	cf;
	cf.lStructSize	= sizeof( CHOOSEFONT );
	cf.hwndOwner	= NULL;
	cf.hDC			= NULL;
	cf.lpLogFont	= &m_lf;
	cf.Flags		= CF_BOTH; 

	if( ChooseFont( &cf )==FALSE )
		return;

	CChCar toto;
	if( toto.DoModal() == IDCANCEL )
		return;

	m_Carac.Format(" ");

	// Appel pour initialiser m_TotalSize
	CDC	*dc;
	dc = GetDC();
	OnDraw(dc);

	// Affiche la taille
	CString aa;
	aa.Format("Largeur=%d Hauteur=%d",m_TotalSize.cx,m_TotalSize.cy);
	((CMainFrame*)AfxGetMainWnd())->m_wndStatusBar.SetPaneText(0,aa);
	AfxMessageBox(aa);

	CString Titre;
	int i;
	for( i=0; i<toto.m_Chaine.GetLength(); i++ )
	{
		unsigned char Byte;
		Byte = toto.m_Chaine[i];
		Titre.Format("%s\\%02x.bmp",toto.m_Dest,(unsigned short)Byte);
		m_Carac.Format("%c",toto.m_Chaine[i]);

		// Appel pour initialiser m_TotalSize
		CDC	*dc;
		dc = GetDC();
		OnDraw(dc);

		// Enregistre Bmp
		SavePageToBmp( Titre );
	}
	if (toto.m_Digit == TRUE) 
	{
		char c;
		for (c = '0'; c <= '9'; c++)
		{
			Titre.Format("%s\\%02x.bmp",toto.m_Dest,(unsigned short)c);
			m_Carac.Format("%c",c);

			// Appel pour initialiser m_TotalSize
			CDC	*dc;
			dc = GetDC();
			OnDraw(dc);

			// Enregistre Bmp
			SavePageToBmp( Titre );
		}
	}
	if (toto.m_UpCase == TRUE) 
	{
		char c;
		for (c = 'A'; c <= 'Z'; c++)
		{
			Titre.Format("%s\\%02x.bmp",toto.m_Dest,(unsigned short)c);
			m_Carac.Format("%c",c);

			// Appel pour initialiser m_TotalSize
			CDC	*dc;
			dc = GetDC();
			OnDraw(dc);

			// Enregistre Bmp
			SavePageToBmp( Titre );
		}
	}
	if (toto.m_LowCase == TRUE) 
	{
		char c;
		for (c = 'a'; c <= 'z'; c++)
		{
			Titre.Format("%s\\%02x.bmp",toto.m_Dest,(unsigned short)c);
			m_Carac.Format("%c",c);

			// Appel pour initialiser m_TotalSize
			CDC	*dc;
			dc = GetDC();
			OnDraw(dc);

			// Enregistre Bmp
			SavePageToBmp( Titre );
		}
	}
}

void CBmpCarView::SavePageToBmp( const CString& FileName ) 
{
	CDC					MemoryDC;
	CBitmap				MemoryBmp;
	PBITMAPINFO			BitmapInfo;
	BITMAPFILEHEADER	BitmapFileHeader;
	LPBYTE				MemoryBits=NULL;
	int					NbColors;

	SaveDC( &MemoryDC, &MemoryBmp );
	BitmapInfo = InitBITMAPINFO( MemoryBmp, NbColors );
	InitBITMAPFILEHEADER( BitmapFileHeader, BitmapInfo, NbColors );
	MemoryBits = GetDIBBits( MemoryDC, BitmapInfo, MemoryBmp );
	BITMAPINFOtoFile( FileName, BitmapFileHeader, BitmapInfo, MemoryBits, NbColors );
	delete [] BitmapInfo;
	delete [] MemoryBits;
}

void CBmpCarView::SaveDC( CDC* MemoryDC, CBitmap* MemoryBmp ) 
{
//	CSize	    TotalSize;
	CClientDC	dc(this);

	ASSERT( MemoryDC );
	ASSERT( MemoryBmp );
	OnPrepareDC( &dc );

//	TotalSize = GetTotalSize();

	if ( !MemoryDC->CreateCompatibleDC( &dc ) ) { ASSERT(FALSE); }

	if ( !MemoryBmp->CreateCompatibleBitmap(&dc, m_TotalSize.cx, m_TotalSize.cy ) ) {ASSERT(FALSE); }
	if ( !MemoryDC->SelectObject(MemoryBmp) ) { ASSERT( FALSE ); }
	MemoryDC->PatBlt(0,0,m_TotalSize.cx, m_TotalSize.cy, WHITENESS);
	OnDraw(MemoryDC);
}

void CBmpCarView::InitBITMAPFILEHEADER( BITMAPFILEHEADER& BitmapFileHeader, BITMAPINFO* BitmapInfo, int NbColors )
{
    BitmapFileHeader.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"
 
    // Calcule la taille du fichier 
    BitmapFileHeader.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
                              BitmapInfo->bmiHeader.biSize + 
							  NbColors*sizeof(RGBQUAD) +
							  BitmapInfo->bmiHeader.biSizeImage); 
 
    BitmapFileHeader.bfReserved1 = 0; 
    BitmapFileHeader.bfReserved2 = 0; 
 
    // Calcul de l'offset du tableau d'indices de couleurs 
    BitmapFileHeader.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
                                  BitmapInfo->bmiHeader.biSize +
								  NbColors*sizeof(RGBQUAD); 
}

PBITMAPINFO CBmpCarView::InitBITMAPINFO( const CBitmap& MemoryBmp, int& NbColors )
{
	PBITMAPINFO BitmapInfo;
	WORD	    BitsPerPixel;
	BITMAP      Bitmap;

	// On r�cup�re le format de couleur et la taille
	MemoryBmp.GetObject( sizeof(BITMAP), &Bitmap );

	// Conversion du format de couleur vers le nombre de bits
    BitsPerPixel = (WORD)(Bitmap.bmPlanes * Bitmap.bmBitsPixel); 
 
    if (BitsPerPixel == 1) 
        BitsPerPixel = 1; 
    else if (BitsPerPixel <= 4) 
        BitsPerPixel = 4; 
    else if (BitsPerPixel <= 8) 
        BitsPerPixel = 8; 
    else if (BitsPerPixel <= 16) 
        BitsPerPixel = 16; 
    else if (BitsPerPixel <= 24) 
        BitsPerPixel = 24; 
    else 
        BitsPerPixel = 32;
   
	NbColors = (int)pow( 2, BitsPerPixel );
	//Reservation m�moire pour BitmapInfo
	if (BitsPerPixel != 24) 
         BitmapInfo = (PBITMAPINFO)new char*[ sizeof(BITMAPINFOHEADER)+sizeof(RGBQUAD)*(NbColors) ]; 
//       BitmapInfo = (PBITMAPINFO)new  PTR[sizeof(BITMAPINFOHEADER)+sizeof(RGBQUAD)*(NbColors)]; 
    else  // Pas de RGBQUAD pour le format 24 bit-par-pixel
         BitmapInfo = (PBITMAPINFO)new char*[sizeof(BITMAPINFOHEADER)]; 
//       BitmapInfo = (PBITMAPINFO)new PTR[sizeof(BITMAPINFOHEADER)]; 

    // Initialisation de BITMAPINFO 
    BitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    BitmapInfo->bmiHeader.biWidth = Bitmap.bmWidth; 
    BitmapInfo->bmiHeader.biHeight = Bitmap.bmHeight; 
    BitmapInfo->bmiHeader.biPlanes = Bitmap.bmPlanes; 
    BitmapInfo->bmiHeader.biBitCount = Bitmap.bmBitsPixel; 
    if (BitsPerPixel < 24) 
        BitmapInfo->bmiHeader.biClrUsed = NbColors; 
 
    // Si la bitmap n'est pas compress�, mettre BI_RGB 
    BitmapInfo->bmiHeader.biCompression = BI_RGB; 
 
    // Calcul du nombre d'octets du tableau des indices de couleur  
    // et stockage dans biSizeImage. 
    BitmapInfo->bmiHeader.biSizeImage = (BitmapInfo->bmiHeader.biWidth + 7) /8 
										* BitmapInfo->bmiHeader.biHeight 
										* BitsPerPixel; 
 
    // Mettre biClrImportant � 0 --> tout les device de couleurs sont importants 
    BitmapInfo->bmiHeader.biClrImportant  = 0; 
    BitmapInfo->bmiHeader.biXPelsPerMeter = 0; 
    BitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	
	return BitmapInfo;
}

LPBYTE CBmpCarView::GetDIBBits( const CDC& MemoryDC, BITMAPINFO* BitmapInfo, const CBitmap& MemoryBmp )
{
	LPBYTE	MemoryBits;

	MemoryBits = new BYTE[ BitmapInfo->bmiHeader.biSizeImage ];

    // Retrouve les octets du DIB
    if (!::GetDIBits(MemoryDC.m_hDC, (HBITMAP)MemoryBmp, 0, (WORD)BitmapInfo->bmiHeader.biHeight, 
                     MemoryBits, BitmapInfo, DIB_RGB_COLORS)) 
       { ASSERT(FALSE); } 
	
	return MemoryBits;
}

void CBmpCarView::BITMAPINFOtoFile( const CString& FileName, const BITMAPFILEHEADER& BitmapFileHeader, BITMAPINFO* BitmapInfo, LPBYTE MemoryBits, int NbColors )
{
	CFile File;
	CFileException e;
	DWORD Total;
	DWORD cb;
	BYTE* hp;

	#define	MAXWRITE	1024
	
	if( !File.Open( (const char*)FileName, CFile::modeCreate | CFile::modeWrite, &e ) ) { ASSERT( FALSE ); }
	File.Write( &BitmapFileHeader, sizeof(BITMAPFILEHEADER) );
	File.Write( BitmapInfo, BitmapInfo->bmiHeader.biSize + NbColors*sizeof(RGBQUAD) );
	
	Total = cb = BitmapInfo->bmiHeader.biSizeImage; 
	hp = MemoryBits; 
	while (cb > MAXWRITE)
	{ 
		File.Write( hp, MAXWRITE );
		cb -= MAXWRITE; 
		hp += MAXWRITE; 
	} 
	File.Write( hp, (int)cb );
	File.Close();
}
