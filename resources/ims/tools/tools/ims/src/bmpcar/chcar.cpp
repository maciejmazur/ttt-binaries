// ChCar.cpp : implementation file
//

#include "stdafx.h"
#include "BmpCar.h"
#include "ChCar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChCar dialog


CChCar::CChCar(CWnd* pParent /*=NULL*/)
	: CDialog(CChCar::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChCar)
	m_Chaine = _T("");
	m_LowCase = FALSE;
	m_UpCase = FALSE;
	m_Digit = FALSE;
	m_Dest = _T("");
	//}}AFX_DATA_INIT
}


void CChCar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChCar)
	DDX_Text(pDX, IDC_EDIT1, m_Chaine);
	DDX_Check(pDX, IDC_LOWCASE, m_LowCase);
	DDX_Check(pDX, IDC_UPCASE, m_UpCase);
	DDX_Check(pDX, IDC_DIGIT, m_Digit);
	DDX_Text(pDX, IDC_EDIT2, m_Dest);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChCar, CDialog)
	//{{AFX_MSG_MAP(CChCar)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChCar message handlers
