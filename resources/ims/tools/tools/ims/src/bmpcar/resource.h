//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BMPCAR.RC
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_BMPCARTYPE                  129
#define IDD_DIALOG1                     130
#define IDC_EDIT1                       1000
#define IDC_UPCASE                      1001
#define IDC_LOWCASE                     1002
#define IDC_DIGIT                       1003
#define IDC_EDIT2                       1004
#define ID_GO                           32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
