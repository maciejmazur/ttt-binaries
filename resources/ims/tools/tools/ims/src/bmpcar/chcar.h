// ChCar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChCar dialog

class CChCar : public CDialog
{
// Construction
public:
	CChCar(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChCar)
	enum { IDD = IDD_DIALOG1 };
	CString	m_Chaine;
	BOOL	m_LowCase;
	BOOL	m_UpCase;
	BOOL	m_Digit;
	CString	m_Dest;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChCar)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChCar)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
