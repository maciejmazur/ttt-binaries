from re import *
from sys import *
import copy

StateX0ToFind = {}
StateX1ToFind = { "1005" : "Fin calcul MG",
                  "1008" : "Coupe tk",      
                }

StateX2ToFind = { "2003" : "Encode tk",
                  "2028" : "Fin encode tk", 
                }

StateX3ToFind = { "3002" : "Calcul MG",
                  "3004" : "Print tk",
                  "3006" : "Fin print tk",
                  "3007" : "Calcul MG",
                  "3271" : "Calcul MG",     
                }
StateX4ToFind = {}
StateX5ToFind = {}
StateX6ToFind = {}
StateX7ToFind = {}

StateXToFind = [StateX0ToFind, StateX1ToFind, StateX2ToFind, StateX3ToFind, StateX4ToFind, StateX5ToFind, StateX6ToFind, StateX7ToFind]

StateX0Counter = {}
StateX1Counter = { "1005" : 1,
                   "1008" : 1,      
                 }

StateX2Counter = { "2003" : 1,
                   "2028" : 1, 
                 }

StateX3Counter = { "3002" : 2,
                   "3004" : 1,
                   "3006" : 1,
                   "3007" : 3,
                   "3271" : 3,     
                 }
StateX4Counter = {}
StateX5Counter = {}
StateX6Counter = {}
StateX7Counter = {}

StateXCounter = [StateX0Counter, StateX1Counter, StateX2Counter, StateX3Counter, StateX4Counter, StateX5Counter, StateX6Counter, StateX7Counter]





def ParseGrafcetTraces (handle, handleLog):
   ColStateX0=[]
   ColStateX1=[]
   ColStateX2=[]
   ColStateX3=[]
   ColStateX4=[]
   ColStateX5=[]
   ColStateX6=[]
   ColStateX7=[]

   line = handle.readline()

   CurrentStatesFound  = ["", "", "", "", "", "", "", ""]
   PreviousStatesFound = ["", "", "", "", "", "", "", ""]

   # file is not empty
   while (line != ""):
      regexp = match("^(.*)[\s]+(?P<StateX0>[0-9]{4})[\s]+(?P<StateX1>[0-9]{4})[\s]+(?P<StateX2>[0-9]{4})[\s]+(?P<StateX3>[0-9]{4})[\s]+(?P<StateX4>[0-9]{4})[\s]+(?P<StateX5>[0-9]{4})[\s]+(?P<StateX6>[0-9]{4})[\s]+(?P<StateX7>[0-9]{4})", line)
      if regexp != None:
         #backup previous states
         PreviousStatesFound = copy.deepcopy(CurrentStatesFound)

         #construct current states
         for col in range(8):
            if ( StateXToFind[col].has_key(eval("regexp.group('StateX%d')" % (col))) == True):
               CurrentStatesFound[col] = eval("regexp.group('StateX%d')" % (col))
            else:
               CurrentStatesFound[col] = ""

         #check for new state to found
         for col in range(8):
            if CurrentStatesFound[col] not in PreviousStatesFound:
               handleLog.write("Select %s: %s %d\n" % (CurrentStatesFound[col], StateXToFind[col][CurrentStatesFound[col]], StateXCounter[col][CurrentStatesFound[col]]))

               StateXCounter[col][CurrentStatesFound[col]] += 1

      else:
         handleLog.write(line+"\n")

      line = handle.readline()

###############################
print argv
if len(argv) == 2:
   fileName = argv[1]
else:
   fileName = '..\\avec logo new.txt'

print "Open file '%s'\n" % (fileName)
handle = file(fileName, 'r')
handleLog = file(fileName+'.log', 'w')

print "Parsing file...\n"
data = ParseGrafcetTraces(handle, handleLog)

handle.close()
handleLog.close()

print "\nFile parsed !\nResults store in '%s.log' file" % fileName

