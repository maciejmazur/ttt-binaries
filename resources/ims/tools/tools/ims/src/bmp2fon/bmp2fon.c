#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <io.h>
#include <windows.h>

#define TR	1

extern short InvMot(short);
extern long InvLong(long);

char	*ProgName = "bmp2fon";

extern short	flgTrace;

typedef struct {
	char	szType[2];
	long	lFile;
	long	ruf1;
	long	lHeader;
	long	ruf2;
	long	lLargeur;
	long	lHauteur;
	long	ruf3[8];
}__attribute__((packed)) BMP_header;

typedef struct {
	char		szVersion[10];
	unsigned short	usNbColMax;
	unsigned short	usNbLigne;
	unsigned short	usSizeChar;
	unsigned short	usSizeCol;
	unsigned char	bIndirectTable[256];
	unsigned char	bWidthTable[256];
}__attribute__((packed)) FON_header;

static unsigned char	bExistTab[256];

short bitmap(char *filename)
{
	const	unsigned short	usMaskTab[] =
		{ 0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
		  0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080 };
	short	i,j,k;
	FILE	*in;
	FILE	*out;
	char	path[80];
	BMP_header	bmp;
	FON_header	fon;
	struct _finddata_t	BMP_ffblk = { 0, };
	int handle;
	unsigned char	bPos;
	unsigned short	usChar;
	unsigned short  *pData;
	unsigned long	ulData;
	unsigned long	ulMask;
	unsigned char	bData;

	printf("Police : %s\n", filename);

	memset(&bExistTab,0,sizeof bExistTab);
	memset(&fon,0,sizeof fon);
	sprintf(fon.szVersion,"%8.8s",filename);

	sprintf(path,"%s\\??.bmp",filename);
	handle = _findfirst(path,&BMP_ffblk);

	if (handle == -1) {
		printf("%s : no BMP found at %s\n",ProgName,path);
		return -2;
	}

	do
	{
		sscanf(BMP_ffblk.name,"%2x",&usChar);
		bExistTab[usChar] = 1;
		sprintf(path,"%s\\%s",filename,BMP_ffblk.name);

		in = fopen(path,"r+b");
		if (in == NULL)
		{
			printf("%s : can't open %s\n",ProgName,path);
			return -2;
		}

		fread(&bmp,1,sizeof bmp,in);	/* BMP header */

		(void)fclose(in);

		if (bmp.szType[0] != 'B' 
		 || bmp.szType[1] != 'M')
		{
			printf("%s : %s isn't a BMP file\n",ProgName,path);
			return -2;
		}

		fon.usNbColMax =  max(fon.usNbColMax,	bmp.lLargeur);
		fon.usNbLigne =   max(fon.usNbLigne,	bmp.lHauteur);
		fon.usSizeCol =   max(fon.usSizeCol,	(bmp.lHauteur+15)/16);
		fon.usSizeChar =  max(fon.usSizeChar,	fon.usSizeCol * bmp.lLargeur);
    }
    while ( _findnext(handle, &BMP_ffblk) == 0 );

   _findclose(handle);
	pData = malloc(2*fon.usSizeChar);
	if (pData == NULL)
	{
		printf("%s : Not enough memory\n",ProgName);
		return -3;
	}

	sprintf(path,"%s.fon",filename);
	out = fopen(path,"w+b");
	if (out == NULL)
	{
		printf("%s : can't open %s\n",ProgName,path);
		return -2;
	}
	fseek(out,sizeof fon,SEEK_SET);

	for (usChar = 0, bPos = 0; usChar < 256; usChar++)
	{
		if (bExistTab[usChar])
        {
			sprintf(path,"%s\\%02x.bmp",filename,usChar);

			in = fopen(path,"r+b");

			if (in == NULL)
			{	
				printf("%s : can't open %s\n",ProgName,path);
				return -2;
			}

			fread(&bmp,1,sizeof bmp,in);	/* BMP header */

            if ( bmp.lHeader == 0x00040036 ) // Le bitmap est un 32 bits et on ne sait pas le g�rer. Inutile d'aller plus loin.
            {
                free( pData );
                printf( "Bitmaps are 32 bits deep and can not be handled, font failed to be created." );
                return -4;
            }

			printf("%c",(unsigned char) usChar);
			/* printf("%ld %ld\n", bmp.lLargeur, bmp.lHauteur); */

			memset(pData,0,2*fon.usSizeChar);
			fseek(in,bmp.lHeader,SEEK_SET);

			if (bmp.lHeader == 0x3E) // Le bitmap est un noir et blanc
			{
				for (i = 0; i < bmp.lHauteur; i++)
				{
					for (j = 0; j < (bmp.lLargeur+31)/32; j++)
					{
						fread(&ulData,1,sizeof ulData,in);
						ulData = InvLong(ulData);
						ulData = ~ulData;
						ulMask = 0x80000000L;
						for (k = 32*j; k < 32*j+32; k++)
						{
							if (k < bmp.lLargeur)
							{
								if (ulMask & ulData)
								{
									*(pData + k*fon.usSizeCol + i/16) |= usMaskTab[i%16];
								}
							}
							ulMask /= 2;
						}
					}
				}
			}
			else // Le bitmap est ** a priori ** en 8 bits (256 couleurs)
			{
				for (i = 0; i < bmp.lHauteur; i++)
				{
					for (j = 0; j < (bmp.lLargeur+3)/4; j++)
					{
						for (k = 4*j; k < 4*j+4; k++)
						{
							fread(&bData,1,sizeof bData,in);
							if (k < bmp.lLargeur)
							{
								if (!bData)
								{
									*(pData + k*fon.usSizeCol + i/16) |= usMaskTab[i%16];
								}
							}
						}
					}
				}
			}

			(void) fclose(in);

			fwrite(pData,1,2*fon.usSizeChar,out);

			if (flgTrace)
			{
				putc('-',stderr);
				putc(usChar,stderr);
				for (i = 2; i < fon.usNbLigne; i++)
					putc('-',stderr);
				putc('\n',stderr);

				for (i = 0; i < bmp.lLargeur; i++)
				{
					for (j = 0; j < fon.usSizeCol; j++)
					{
						for (k = 0; k < 16; k++)
						{
							if (16*j+k < fon.usNbLigne)
							{
								if (usMaskTab[k] & *(pData + i * fon.usSizeCol + j))
									putc('X',stderr);
								else
									putc(' ',stderr);
							}
						}
					}
					putc('\n',stderr);
				}	
			}

			fon.bIndirectTable[usChar] = bPos;
			if (bPos == 0)
			{	
				memset(fon.bWidthTable,(int)bmp.lLargeur,sizeof fon.bWidthTable);
			}
			else 
			{
				fon.bWidthTable[usChar] = bmp.lLargeur;
			}

			bPos++;
		}
	}


	if (flgTrace)
	{
		for (i = 0; i < fon.usNbLigne; i++)
			putc('+',stderr);
		putc('\n',stderr);
	}

	fseek(out,0,SEEK_SET);
	fon.usNbColMax = InvMot(fon.usNbColMax);
	fon.usNbLigne =  InvMot(fon.usNbLigne);
	fon.usSizeChar = InvMot(fon.usSizeChar);
	fon.usSizeCol =  InvMot(fon.usSizeCol);
	fwrite(&fon,1,sizeof fon,out);

	printf(" Taille:%ld\n\n",filelength(fileno(out)));

	(void) fclose(out);


	return 0;
}
