#include <stdio.h>
#include <ctype.h>
#include <string.h>

extern	char	*ProgName;

extern	short	bitmap(char *);

short	flgTrace = 0;
short	flgAscii = 0;

int main(int argc, char *argv[])
{
	int	arg;

	if (argc == 1)
		goto usage;
	else
	{
		arg = 1;
		if (argv[arg][0] == '-' || argv[arg][0] == '/')
		{
			switch (argv[arg][1])
			{
			case 'v' :
			case 'V' :	
				flgTrace = 1;
				return bitmap(argv[2]);
			case '?' :
			case 'h' :
			case 'H' :
			default  : goto usage;
			}
		}
		else return bitmap(argv[1]);
	}
usage:	
	printf("%s filename\t\n",ProgName);
	return 0;
}
