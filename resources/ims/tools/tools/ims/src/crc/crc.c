#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/* Calculate CRC  */
static const uint16_t crctbl[16] =
{
   0x0000,0x1081,
   0x2102,0x3183,
   0x4204,0x5285,
   0x6306,0x7387,
   0x8408,0x9489,
   0xA50A,0xB58B,
   0xC60C,0xD68D,
   0xE70E,0xF78F
};

uint16_t CalcCrc16(void *pData, uint32_t ulLen)
{
uint32_t  ulCnt;
uint16_t usCrc = 0;
uint8_t	 *pbData = (uint8_t *)pData;

   for (ulCnt=0; ulCnt<ulLen; ulCnt++) {
      usCrc = ( usCrc >> 4 )^crctbl[( *pbData ^ usCrc ) & 0x0F ];
      usCrc = ( usCrc >> 4 )^crctbl[(( *pbData++ >> 4) ^ usCrc ) & 0x0F ];
   }
   return(usCrc);
}

void usage(const char *name)
{
	printf("Usage:\n%s <binary filename> <crcfilename>\n", name);
}

int main(int argc, char *argv[])
{
	int fd;
	int filesize;
	struct stat filestat;
	unsigned char *data = NULL;
	uint16_t crc = 0;
	unsigned char crcbuftofile[64];
	
	if(argc != 3)
	{
		usage(argv[0]);
		return 2;
	}
	if(access(argv[1], R_OK))
	{
		printf("Cannot open file %s for reading\n", argv[1]);
		usage(argv[0]);
		return 2;
	}
	if(stat(argv[1], &filestat))
	{
		printf("Cannot get statistcs on file %s\n", argv[1]);
		usage(argv[0]);
		return 2;
	}
	filesize = filestat.st_size;
	data = (unsigned char *)malloc(filesize);
	if(!data)
	{
		printf("Cannot alloc memory to read %d bytes\n", filesize);
		usage(argv[0]);
		return 2;
	}
	fd = open(argv[1], O_RDONLY | O_BINARY);
	if(read(fd, data, filesize) != filesize)
	{
		free(data);
		close(fd);
		return 2;
	}
	close(fd);
	crc = CalcCrc16(data, filesize);
	free(data);
	fd = open(argv[2], O_RDWR | O_CREAT);
	if(fd == -1)
	{
		printf("Cannot open crc file %s\n", argv[2]);
		close(fd);
		goto finalize;
	}
	memset(crcbuftofile, 0, sizeof(crcbuftofile));
	sprintf(crcbuftofile, "CRC = 0x%04X\n", crc);
	if(write(fd, crcbuftofile, strlen(crcbuftofile)) != strlen(crcbuftofile))
	{
		printf("Cannot write in crc file %s\n", argv[2]);
	}
	close(fd);
finalize:
	printf("CRC = %04X\n", crc);
	return 0;
}

