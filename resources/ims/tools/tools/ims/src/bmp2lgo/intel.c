#if PC

/*컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�*/
/*                             INTEL.C                                   */
/*컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�*/
/* Routines de conversion de format intel vers motorola (et inversement) */


short InvMot(short entier);
long  InvLong(long entier_long);


/*
 * ------------------------------------------------------------
 * nom     : InvMot
 * syntaxe : short InvMot(short val);
 * role    : inversion poids forts poids faibles d'un entier pour le PC
 * retour  : entier retourne
 * ------------------------------------------------------------
 */
short InvMot(val)
short val;
{
return ( ((val << 8) & 0xFF00) + ((val >> 8) & 0xFF) );
} /* short InvMot(short val) */

/*
 * ------------------------------------------------------------
 * nom     : InvLong
 * syntaxe : long InvLong(long val);
 * role    : inversion d'un entier long pour le PC
 * retour  :
 *           long retourne
 * ------------------------------------------------------------
 */
long InvLong(val)
long val;
{
union {
  long lval;
  short int val[2];
  } u;
short int i;

u.lval = val;

i = u.val[1];

u.val[1] = InvMot(u.val[0]);
u.val[0] = InvMot(i);

return u.lval;
} /* long InvMot(long val) */

#endif
