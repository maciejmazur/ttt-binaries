
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


extern short InvMot(short);
extern long InvLong(long);

char	*ProgName = "bmp2lgo";

extern short flgTrace;

typedef struct {
	char	szType[2];
	long	lFile;
	long	ruf1;
	long	lHeader;
	long	ruf2;
	long	lLargeur;
	long	lHauteur;
	long	ruf3[8];
}__attribute__((packed)) BMP_header;

typedef struct {
	char		szVersion[10];
	unsigned short	usNbCol;
	unsigned short	usNbLigne;
	unsigned short	usSizeChar;
	unsigned short	usSizeCol;
} LGO_header;

short bitmap(char *filename)
{
	const	unsigned short	usMaskTab[] =
		{ 0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
		  0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080 };
	short	i,j,k;
	FILE	*in;
	FILE	*out;
	char	path[80];
	BMP_header	bmp;
	LGO_header	lgo;
	unsigned short  *pData;
	unsigned long	ulData;
	unsigned long	ulMask;
	unsigned char	bData;

	memset(&lgo,0,sizeof lgo);
	sprintf(lgo.szVersion,"%8.8s",filename);

	sprintf(path,"%s.bmp",filename);
	in = fopen(path,"r+b");
	if (in == NULL)
	{
		printf("%s : can't open %s\n",ProgName,path);
		return -2;
	}

	fread(&bmp,1,sizeof bmp,in);	/* BMP header */

	if (bmp.szType[0] != 'B'
	 || bmp.szType[1] != 'M')
	{
		printf("%s : %s isn't a BMP file\n",ProgName,path);
		return -2;
	}

/*
	if (bmp.lHeader != 62L)
	{
		printf("%s : %s isn't a black & white BMP\n",ProgName,path);
		return -2;
	}
*/
    
    if ( bmp.lHeader == 0x00040036 ) // Le bitmap est un 32 bits, on ne sait pas le g�rer.
    {
        printf( "Bitmap is 32 bits deep and can not be handled, logo failed to be created." );
        return -4;
    }

	printf("%ld %ld\n", bmp.lLargeur, bmp.lHauteur);

	lgo.usNbCol =     bmp.lLargeur;
	lgo.usNbLigne =   bmp.lHauteur;
	lgo.usSizeCol =   (bmp.lHauteur+15)/16;
	lgo.usSizeChar =  lgo.usSizeCol * bmp.lLargeur;

	pData = malloc(2*lgo.usSizeChar);
	if (pData == NULL)
	{
		printf("%s : Not enough memory\n",ProgName);
		return -3;
	}

	memset(pData,0,2*lgo.usSizeChar);
	fseek(in,bmp.lHeader,SEEK_SET);

	if (bmp.lHeader == 62L)
	{
		/* Black&White Bitmap */
		for (i = 0; i < bmp.lHauteur; i++)
		{
			for (j = 0; j < (bmp.lLargeur+31)/32; j++)
			{
				fread(&ulData,1,sizeof ulData,in);
				ulData = InvLong(ulData);
				ulData = ~ulData;
				ulMask = 0x80000000L;
				for (k = 32*j; k < 32*j+32; k++)
				{
					if (k < bmp.lLargeur)
					{
						if (ulMask & ulData)
						{
							*(pData + k*lgo.usSizeCol + i/16) |= usMaskTab[i%16];
						}
					}
					ulMask /= 2;
				}
			}
		}
	}
	else
	{
		/* 256 Colors Bitmap */
		for (i = 0; i < bmp.lHauteur; i++)
		{
			for (j = 0; j < (bmp.lLargeur+3)/4; j++)
			{
				for (k = 4*j; k < 4*j+4; k++)
				{
					fread(&bData,1,sizeof bData,in);
					if (k < bmp.lLargeur)
					{
						if (!bData)
						{
							*(pData + k*lgo.usSizeCol + i/16) |= usMaskTab[i%16];
						}
					}
				}
			}
		}
	}

	(void) fclose(in);

	if (flgTrace)
	{
		for (i = 0; i < bmp.lLargeur; i++)
		{
			for (j = 0; j < lgo.usSizeCol; j++)
			{
				for (k = 0; k < 16; k++)
				{
					if (16*j+k < lgo.usNbLigne)
					{
						if (usMaskTab[k] & *(pData + i * lgo.usSizeCol + j))
							putc('X',stderr);
						else
							putc(' ',stderr);
					}
				}	
			}
			putc('\n',stderr);
		}
	}

	sprintf(path,"%s.lgo",filename);
	out = fopen(path,"w+b");
	if (out == NULL)
	{
		printf("%s : can't open %s\n",ProgName,path);
		return -2;
	}

	fseek(out,sizeof lgo,SEEK_SET);
	fwrite(pData,1,2*lgo.usSizeChar,out);

	fseek(out,0,SEEK_SET);
	lgo.usNbCol =    InvMot(lgo.usNbCol);
	lgo.usNbLigne =  InvMot(lgo.usNbLigne);
	lgo.usSizeChar = InvMot(lgo.usSizeChar);
	lgo.usSizeCol =  InvMot(lgo.usSizeCol);
	fwrite(&lgo,1,sizeof lgo,out);

	(void) fclose(out);

	return 0;
}
