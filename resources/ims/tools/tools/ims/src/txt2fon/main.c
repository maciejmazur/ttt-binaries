#include <stdio.h>
#include <ctype.h>
#include <string.h>

extern	char	*ProgName;

extern	short	bitmap(char *, char *, char *);

short	flgAscii = 0;

int main(int argc, char *argv[])
{
	int	arg;
	char *source;
	char *target;
	char *name;

	if (argc < 4 || argc > 5)
	{
		goto usage;
	}
	else
	{
		if(argc == 5)
		{
         target = argv[4];
		}
      else
      {
         target = 0;
      }
      name = argv[1];
		arg = 2;
		if (argv[arg][0] == '-' || argv[arg][0] == '/')
		{
			switch (argv[arg][1])
			{
			case '?' :
			case 'h' :
			case 'H' :
				goto usage;
			case 'm' :
			case 'M' :
				flgAscii = 1;
			case 'b' :
			case 'B' :
				source = argv[3];
				break;
			default  : goto usage;
			}
		}
		else goto usage;
	}
	return bitmap(name, source, target);
usage:	
	printf("%s fontName /B source [target] (binary)\n",ProgName);
	printf("%s fontName /M source [target] (Motorola)\n",ProgName);
	return 0;
}
