
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

extern short InvMot(short);
extern long InvLong(long);

extern	short	flgAscii;

#define INV 1

char	*ProgName = "txt2font";

unsigned char	Data[32000];
unsigned char	Indirect[256];
unsigned char	Largeur[256];
unsigned char	Comment[256];


short bitmap(char *name, char *filename, char *target)
{
	short	i,j,k;
	FILE	*in;
	FILE	*out;
	char  szVersion[ 10];
	char	path[80];
	char    line[132];
	short	hauteur;
	short   hauteur16;
	short	largeur;
	short	largeur_max;
	short	position;
	short	tmp;
	unsigned char	*ptr;
	unsigned char	c;
	unsigned short	*ptrUS;

	memset(Data,0,sizeof Data);
	memset(Indirect,0,sizeof Indirect);
	memset(Comment,0,sizeof Comment);

	memset( szVersion, 0, sizeof szVersion);
	sprintf( szVersion, "%8.8s", name);

	in = fopen(filename,"rt");
	if (in == NULL)
	{
		printf("%s : can't open %s\n",ProgName,filename);
		return -2;
	}

	printf("--- Fichier : %s\n",filename);

	fgets(line, sizeof line, in);
	for (hauteur = 2; line[hauteur] == '-'; hauteur++);
	hauteur16 = (hauteur + 15) / 16;
	printf("\thauteur   %d\n",hauteur);
	printf("\thauteur16 %d\n",hauteur16);

        ptr = Data;
	position = 0;
        while (line[0] == '-')
	{
		c = line[1];
		printf("%c",c);
		Comment[position] = c;		
		Indirect[c] = (unsigned char) position;
		largeur = 0;
		fgets(line, sizeof line, in);
		/* Creation du bitmap */
		while (line[0] == ' ' || line[0] == 'X')
		{
			largeur ++;
			for (i = 0; i < hauteur; i++)
			{
				if (line[i] == 'X')
				{
					if (flgAscii)
                  /* L'inversion des octets se fera par le sprintf au format intel */
						*(ptr+i/8) |= (0x01 << (i%8));
					else
					{
                  /* Pour l'image binaire il faut retourner les octets dans le mot */
						if( (i/8)%2 == 0)
							*(ptr+i/8+1) |= (0x01 << (i%8));
						else
							*(ptr+i/8-1) |= (0x01 << (i%8));
					}
				}
			}

			fgets(line, sizeof line, in);
			ptr += (2*hauteur16);
		}
		if (position == 0)
		{
			memset(Largeur,largeur,sizeof Largeur);
			largeur_max = largeur;
		}
		else
		{
			Largeur[c] = largeur;
			if (largeur > largeur_max) 
				largeur_max = largeur;
		}
		for (; largeur < hauteur; largeur++)
			ptr += (2*hauteur16);
		position ++;
	}

	fclose(in);

	printf("\n\tlargeur max %d\n\n",largeur_max);

	if (flgAscii)
	{
      if(!target)
      {
         sprintf(path,"%s.src",filename);
         out = fopen(path,"w+t");
      }
      else
      {
         out = fopen(target,"w+t");
      }
		if (out == NULL)
		{
			printf("%s : can't open %s\n",ProgName,path);
         return -1;
		}

		fprintf(out,"\n\n");
		fprintf(out," CHIP 68332,24\n");
		fprintf(out," ALIGN 2\n\n");
		fprintf(out," XDEF _FONT_%s\n\n",name);
		fprintf(out," SECT const\n\n");
		fprintf(out,"_FONT_%s\n",name);

		fprintf(out,"; header\n");
		fprintf(out," dc.b '%8.8s'\n", szVersion);
		fprintf(out," dc.w 0\n");
		fprintf(out," dc.w $%04hX\n",largeur_max);
		fprintf(out," dc.w $%04hX\n",hauteur);
		fprintf(out," dc.w $%04hX\n",largeur_max*hauteur16);
		fprintf(out," dc.w $%04hX\n",hauteur16);

		fprintf(out,"; indirect\n");
		for (i = 0; i < 16; i++)
		{
			fprintf(out," dc.b ");
			for (j = 0; j < 15; j++)	
			{
				fprintf(out,"$%02hX,",(unsigned short)Indirect[16*i+j]);
			}
			fprintf(out,"$%02hX\n",(unsigned short)Indirect[16*i+15]);
		}

		fprintf(out,"; largeur\n");
		for (i = 0; i < 16; i++)
		{
			fprintf(out," dc.b ");
			for (j = 0; j < 15; j++)	
			{
				fprintf(out,"$%02hX,",(unsigned short)Largeur[16*i+j]);
			}
			fprintf(out,"$%02hX\n",(unsigned short)Largeur[16*i+15]);
		}

		for (i = 0, ptr = Data; i < position; i++, ptr += (hauteur*2*hauteur16))
		{
			ptrUS = ptr;
			fprintf(out,"* '%c' \\x%02x \n",Comment[i],(unsigned short)Comment[i]);
			for (j = 0; j < largeur_max; j++)
			{
				fprintf(out," dc.w ");
				for (k = 0; k < hauteur16-1; k++)
				{
           
					fprintf(out,"$%04hX,",*ptrUS++);
				}
				fprintf(out,"$%04hX\n",*ptrUS++);
			}
		}

		fprintf(out,"\n END\n");
	}
	else
	{
      if(!target)
      {
         sprintf(path,"%s.fon",filename);
         out = fopen(path,"w+t");
      }
      else
      {
         out = fopen(target,"w+t");
      }
		if (out == NULL)
		{
			printf("%s : can't open %s\n",ProgName,path);
		return -1;
		}

		fwrite( szVersion, 1, sizeof szVersion, out);
		tmp =InvMot(largeur_max);
		fwrite(&tmp, 1, 2, out);
		tmp = InvMot(hauteur);
		fwrite(&tmp, 1, 2, out);
		tmp =InvMot(largeur_max*hauteur16);
		fwrite(&tmp, 1, 2, out);
		tmp = InvMot(hauteur16);
		fwrite(&tmp, 1, 2, out);

		fwrite(Indirect, 1, sizeof Indirect, out);
		fwrite(Largeur, 1, sizeof Largeur, out);

		for (i = 0, ptr = Data; i < position; i++, ptr += (hauteur*2*hauteur16))
		{
			fwrite(ptr, 1, largeur_max * 2 * hauteur16, out);
		}
	}

	fclose(out);

	return 0;
}

