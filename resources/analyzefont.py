import struct, sys

def extractFontHeader(fontFile):
   content = file(fontFile, "rb").read()
   
   magic = struct.unpack(">4s", content[12:16])[0]
   print repr(magic), magic == "FONT"
   if magic == "FONT":
      extractIml5Font(content, fontFile)
   elif magic is "LOGO":
      print "IML5 logo file..."
   else:
      extractImsFont(content, fontFile)
   
def extractIml5Font(content, fontFile):
   def extractPageInfo(content, index, firstPageExtracted):
      print "++++", list(content[index:index+16]), index
      BMP1, BMP = struct.unpack("<h h", content[index:index+4])
      
      if not BMP1 and not BMP:
         if firstPageExtracted:
            return
         
      offsetNextBmp, nbCol, nbLine, sizechar, sizeCol = struct.unpack("<L H H H H", content[index+4:index+16])
      indirectionTable = struct.unpack("256B", content[index+16:index+272])
      largeurTable = struct.unpack("256B", content[index+272:index+528])
      pageRank = (BMP1<<16) + BMP
      print "PAGE (%d,%d)" % (BMP1, BMP)
      print "offsetNextBmp: %s" % (offsetNextBmp*2)
      print "nbCol: %s" % nbCol
      print "nbLine: %s" % nbLine
      print "sizechar: %s" % sizechar
      print "sizeCol: %s" % sizeCol
      for char, idx in enumerate(indirectionTable):
         if idx:
            print "Chr(x%X): %s graphical index %d, largeur %d" % ((pageRank+char), chr(char), idx, largeurTable[char])
      
      extractPageInfo(content, (index+528+(offsetNextBmp*2)), True)
      
   version, _ = struct.unpack("12s 4s", content[:16])
   
   print "IML5 font file %s" % fontFile
   print "Version: %s" % repr(version)
   
   extractPageInfo(content, 16, False)
         
def extractImsFont(content, fontFile):
   version, nbCol, nbLine, sizechar, sizeCol = struct.unpack(">10s H H H H ", content[:18])
   indirectionTable = struct.unpack(">256B", content[18:274])
   largeurTable = struct.unpack(">256B", content[274:530])
   
   print "IMS font file %s" % fontFile
   print "Version: %s" % repr(version)
   print "nbCol: %s" % nbCol
   print "nbLine: %s" % nbLine
   print "sizechar: %s" % sizechar
   print "sizeCol: %s" % sizeCol
   for char, index in enumerate(indirectionTable):
      if index:
         print "Chr(x%X): %s graphical index %d, largeur %d" % (char, chr(char), index, largeurTable[char])
   
if __name__ == "__main__":
   extractFontHeader(sys.argv[1])
